#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

int main(){
int fd, jugador, puntos;

	if(mkfifo("/tmp/logger_tenis", 0666) < 0){
	perror("Error con FIFO ");
	printf("%d/n", errno);
	return 0;
	}


	fd = open("/tmp/logger_tenis", O_RDONLY);

while(read(fd, &jugador, sizeof(jugador)) == sizeof(jugador)){
	read(fd, &puntos, sizeof(puntos));
	printf("\n El JUGADOR %d MARCÓ. SU CASILLERO ACUMULA %d PUNTOS",jugador, puntos);

	if (puntos == 3){
		printf("\n YA TENEMOS GANADOR, JUGADOR %d\n",jugador);
		close(fd);
		unlink("/tmp/logger_tenis");
	}
}
		close(fd);
		unlink("/tmp/logger_tenis");
}
