#include "DatosMemCompartida.h"

#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <sys/mman.h>

int main(){

	int fd_dmc;

	DatosMemCompartida * datMenCom;
	//DatosMemCompartida d;

	if((fd_dmc = open("/tmp/datos_mem_compartida",O_RDWR)) < 0){
	perror("Error al abrir datos compartidos en BOT ");
	printf("%d/n",errno);
	return 0;
	}
		
ftruncate(fd_dmc, sizeof(DatosMemCompartida));

	if((datMenCom = static_cast<DatosMemCompartida *>(mmap(NULL, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED,fd_dmc, 0))) == MAP_FAILED){
	perror("ERROR AL HACER MMAP en BOT ");
	printf("%d/n",errno);
	return 0;
	}

	close(fd_dmc);
	datMenCom->signal = 1;
	while(datMenCom->signal != 0){

	if(datMenCom->esfera.centro.y > ((datMenCom->raqueta1.y1 + datMenCom->raqueta1.y2)/2)){
		datMenCom->accion = 1;
	}
	else if(datMenCom->esfera.centro.y < ((datMenCom->raqueta1.y1 + datMenCom->raqueta1.y2)/2)){
		datMenCom->accion = -1;
	}
	else {
		datMenCom->accion = 0;
	}
		
	usleep(100000);
	}

return 1;
}

