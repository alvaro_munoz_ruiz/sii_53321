// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "Mundo.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <sys/mman.h>

using namespace std;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	close(fd);
	dmc->signal = 0;
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,620,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();

	for(int j=0; j < esfera.size(); j++)
		esfera[j]->Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);

	
	int i;

	for(i=0;i < esfera.size();i++)	
		esfera[i]->Mueve(0.025f);

	for(i=0;i<paredes.size();i++)
	{
		for(int j=0; j < esfera.size(); j++)		
			paredes[i].Rebota(*esfera[j]);
			paredes[i].Rebota(jugador1);
			paredes[i].Rebota(jugador2);
	}

	for(int j=0; j < esfera.size(); j++){	
		jugador1.Rebota(*esfera[j]);
		jugador2.Rebota(*esfera[j]);
	}

	for(int j=0; j < esfera.size(); j++){	
		if(fondo_izq.Rebota(*esfera[j]))
		{
			int p2 = 2; 			
			esfera[j]->centro.x=0;
			esfera[j]->centro.y=rand()/(float)RAND_MAX;
			esfera[j]->velocidad.x=2+2*rand()/(float)RAND_MAX;
			esfera[j]->velocidad.y=2+2*rand()/(float)RAND_MAX;
			write(fd, &p2, sizeof(int));			
			puntos2++;
			write(fd, &puntos2, sizeof(int));
			if (puntos2  == 3){
				dmc->signal = 0;
				exit(EXIT_SUCCESS);
			}
			esfera[j]->radio=0.5;
			if(esfera.size()<3)
				esfera.push_back(new Esfera);
			}
	}

	for(int j=0; j < esfera.size(); j++){	
		if(fondo_dcho.Rebota(*esfera[j]))
		{
			int p1 = 1;				
			esfera[j]->centro.x=0;
			esfera[j]->centro.y=rand()/(float)RAND_MAX;
			esfera[j]->velocidad.x=-2-2*rand()/(float)RAND_MAX;
			esfera[j]->velocidad.y=-2-2*rand()/(float)RAND_MAX;
			write(fd, &p1, sizeof(int));			
			puntos1++;
			write(fd, &puntos1, sizeof(int));
			if (puntos1  == 3){
				dmc->signal = 0;
				exit(EXIT_SUCCESS);
			}
			esfera[j]->radio=0.5;
			if(esfera.size()<3)			
				esfera.push_back(new Esfera);
		}
	}

	
	dmc->raqueta1=jugador1;
	dmc->esfera=*esfera[0];

	if(dmc->accion == 0) {}
	else if(dmc->accion == -1){
		OnKeyboardDown('s', 0, 0);
	}
	else if(dmc->accion == 1){
		OnKeyboardDown('w', 0, 0);
	}

}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}
}

void CMundo::Init()
{

	esfera.push_back(new Esfera);
	puntos1=0;
	puntos2=0;

//creacion fifo	
	if((fd = open("/tmp/logger_tenis", O_WRONLY)) < 0){
		perror("ERROR CON EL LOGGER %d", errno);
		return;
	}


//fichero DatosMemCompartidos.h
	if((fd_dmc = open("/tmp/datos_mem_compartida",O_CREAT |O_TRUNC| O_RDWR,0600)) < 0){
		perror("ERROR AL COMPARTIR DATOS %d", errno);
		return;
	}

ftruncate(fd_dmc, sizeof(DatosMemCompartida));

	if((dmc = static_cast<DatosMemCompartida *>(mmap(NULL, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED,fd_dmc, 0))) == MAP_FAILED){
	perror("ERROR AL HACER MMAP %d", errno);
	return;
	}
	close(fd_dmc);

	dmc->signal = 1;

	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}
